package sheridan;

public class Farenheit {
	public static int convertFromCelsius(int c)
	{
		return (int)Math.ceil((c * 1.8) + 32);
	}
	
	public static void main(String args[])
	{
		System.out.println(convertFromCelsius(25));
	}
	
}
