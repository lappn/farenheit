package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FarenheitTest {

	@Test
	public void RegularTest()
	{
		int c = 25;
		assertTrue(Farenheit.convertFromCelsius(c) == 77);
	}
	
	@Test
	public void ExceptionTest()
	{
		int c = 25;
		assertFalse(Farenheit.convertFromCelsius(c) == 234);
	}
	
	@Test
	public void BoundaryOutTest()
	{
		int c = 33;
		assertFalse(Farenheit.convertFromCelsius(c) == 91);
	}
	
	@Test
	public void BoundaryInTest()
	{
		int c = 33;
		assertTrue(Farenheit.convertFromCelsius(c) == 92);
	}

}
